flash.ready(function(){

	document.body.querySelectorAll('.video--iframe').forEach(function(video){
		var iframe = video.querySelector('iframe');
		if(iframe) {
			var src = iframe.src;
			var image = video.querySelector('.video__image');
			video.setAttribute('data-src', src);
			if(image) {
				iframe.remove();
			} else {
				iframe.src = iframe.src.replace('autoplay=', 'n_autoplay=');
			}

			var playVideo = function(){
				if(image) {
					image.style.display = 'none';
				} else {	
					iframe.remove();
				}
				video.querySelector('.video__container').insertAdjacentHTML('beforeend', '<iframe src="' + video.getAttribute('data-src') + '&autoplay=1&muted=1&autopause=0"></iframe>');
				video.classList.remove('video--cover');
				video.closest('.video').classList.add('video--playing');
				video.removeEventListener('click', playVideo);
			};

			video.parentNode.addEventListener('click', playVideo);
		}
	});

}, 'layer_video')